# -*- coding: utf-8 -*-
require "test_helper"
require 'essing'

describe "Essing" do
  describe "CsvReader" do
    before do
      @create_abfile = lambda do |sep, nrecs = 2|
        path = "#{$tmpdir}/essing_csv_reader_tmp.csv"

        records = []
        1.upto(nrecs) do |i|
          rec = { :a => "1#{i}", :b => "2#{i}" }
          records.push(rec)
        end

        File.open(path, "wb") do |file|
          file.puts "a#{sep}b"
          records.each{ |r| file.puts "#{r[:a]}#{sep}#{r[:b]}" }
        end
        path
      end

      @create_dupfile = lambda do |sep|
        path = "#{$tmpdir}/essing_csv_reader_tmp.csv"

        File.open(path, "wb") do |file|
          file.puts "a#{sep}a#{sep}a"
          file.puts "1#{sep}2#{sep}3"
        end
        path
      end
    end

    it "should sniff column separator" do
      [ "\t", "|", ";", "," ].each do |sep|
        path = @create_abfile.call(sep, 2)
        expect(Essing::CsvReader.sniff_column_separator(path)).to eq(sep)
      end
    end

    it "should read headers" do
      path = @create_abfile.call("\t", 2)
      expect(Essing::CsvReader.get_headers(path)).to eq([:a, :b])
    end

    it "should normalize headers" do
      path = "#{$tmpdir}/essing_csv_reader_tmp.csv"
      File.open(path, "wb") do |file|
        file.puts [ '"łódź"', '"ala MA kota"', '"a  cc"', '"fooBar"', '"Baz"' ].join(",")
      end
      expect(Essing::CsvReader.get_headers(path)).to eq([:lodz, :ala_ma_kota, :a_cc, :foobar, :baz ])
    end

    it "should name empty headers" do
      path = "#{$tmpdir}/essing_csv_reader_tmp.csv"
      File.open(path, "wb") do |file|
        file.puts [ '"a"', '', '"c"', '""' ].join(",")
      end
      expect(Essing::CsvReader.get_headers(path, skip_blanks: false)).to eq([:a, :xcol_1, :c, :xcol_3 ])
    end

    it "should skip empty headers from the end" do
      path = "#{$tmpdir}/essing_csv_reader_tmp.csv"
      File.open(path, "wb") do |file|
        file.puts [ '"a"', '', '"c"', '""' ].join(",")
      end
      expect(Essing::CsvReader.get_headers(path)).to eq([:a, :xcol_1, :c])
    end

    it "should read csv file" do
      path = @create_abfile.call("\t", 2)
      csv = Essing::CsvReader.new(path)
      line_numbers = []
      csv.each_with_index do |rec, i|
        expect(rec.keys.size).to eq(2)
        expect(rec.keys).to eq([:a, :b])
        line_numbers.push(i)
        if i == 2
          expect(rec).to eq({:a=>"11", :b=>"21"})
        end
      end
      expect(line_numbers).to eq([2,3]) # 1 - headers
    end

    it "should auto suffix duplicates" do
      path = @create_dupfile.call("\t")
      csv = Essing::CsvReader.new(path)
      csv.each_with_index do |rec, i|
        expect(rec.keys.size).to eq(3)
        expect(rec.keys).to eq([:a, :a__1, :a__2])
        expect(rec).to eq({:a=>"1", :a__1=>"2", :a__2 => "3"})
      end
    end
  end
end
