# -*- coding: utf-8 -*-
require "test_helper"
require "essing"
require "axlsx-alt"

describe "Essing" do
  describe "Utils" do
    before :each do
      @root = "/tmp/essing-utils-testing"
      FileUtils.rm_rf(@root)
      FileUtils.mkdir_p(@root)
    end

    it "refinements (internal)" do
      require 'essing/refinements'
      module Foo
        using Essing::Refinements
        module Utils
           def self.internal_test_refinements(p, q)
             p.end_with?(q)
             p.start_with?(q)
             p.include?(q)
           end
        end
      end

      Foo::Utils.send(:internal_test_refinements, :aaaa, :aa)
    end

    it "unpack" do
      # .gz
      fpath = "#{@root}/aa.txt.gz"
      system("echo -n 'aa' | gzip > #{fpath}")

      Essing::Utils.unpack_file(fpath) do |path|
        expect(File.read(path)).to eq('aa')
      end
      expect(File.exist?("#{@root}/aa.txt")).to eq(false)

      # .gz.bz2
      system("cat #{fpath} | bzip2 > #{fpath}.bz2; rm -f #{fpath}")
      fpath = "#{fpath}.bz2"
      Essing::Utils.unpack_file(fpath) do |path|
        expect(File.read(path)).to eq('aa')
      end
      expect(File.exist?("#{@root}/aa.txt.gz")).to eq(false)
      expect(File.exist?("#{@root}/aa.txt")).to eq(false)
      system("rm -f #{fpath}")
    end

    it "unpack#tmpdir" do
      # .gz
      fpath = "#{@root}/aa.txt.gz"
      system("echo -n 'aa' | gzip > #{fpath}")

      tmpdir = "#{@root}/tmp"
      FileUtils.mkdir_p(tmpdir)
      Essing::Utils.unpack_file(fpath, tmpdir: tmpdir) do |path|
        expect(path).to eq("#{tmpdir}/aa.txt")
        expect(File.read(path)).to eq('aa')
        expect(File.exist?("#{@root}/.aa.txt")).to eq(false)
      end
      expect(File.exist?("#{@root}/aa.txt")).to eq(false)
      expect(File.exist?("#{@root}/.aa.txt")).to eq(false)
      # .gz.bz2
      system("cat #{fpath} | bzip2 > #{fpath}.bz2; rm -f #{fpath}")
      fpath = "#{fpath}.bz2"
      Essing::Utils.unpack_file(fpath, tmpdir: tmpdir) do |path|
        expect(path).to eq("#{tmpdir}/aa.txt")
        expect(File.read(path)).to eq('aa')
      end
      expect(File.exist?("#{@root}/aa.txt.gz")).to eq(false)
      expect(File.exist?("#{@root}/aa.txt")).to eq(false)
    end

    it "unpack#xlsx" do
      package = ::Axlsx::Package.new
      package.workbook do |wb|
        wb.add_worksheet(name: "foo") do |sh|
          sh.add_row([ 'Foo', 'Bar' ])
        end

        wb.add_worksheet(name: "bar") do |sh|
          sh.add_row([ 'Bar', 'Foo' ])
        end
      end

      fpath = "#{@root}/aa.xlsx"
      package.serialize fpath
      Essing::Utils.unpack_file(fpath) do |path|
        expect(File.basename(path)).to eq(".aa.csv")
        expect(File.read(path)).to eq("Foo,Bar\n")
      end
      expect(File.exist?("#{@root}/aa.csv")).to eq(false)

      Essing::Utils.unpack_file(fpath, xlsx2csv: { sheet_name: 'foo' }) do |path|
        expect(File.basename(path)).to eq(".aa.csv")
        expect(File.read(path)).to eq("Foo,Bar\n")
      end
      expect(File.exist?("#{@root}/aa.csv")).to eq(false)

      Essing::Utils.unpack_file(fpath, xlsx2csv: { sheet_name: 'bar' }) do |path|
        expect(File.basename(path)).to eq(".aa.csv")
        expect(File.read(path)).to eq("Bar,Foo\n")
      end
      expect(File.exist?("#{@root}/.aa.csv")).to eq(false)
    end

    it "unpack#xlsx.nonhidden" do
      package = ::Axlsx::Package.new
      package.workbook do |wb|
        wb.add_worksheet(name: "foo") do |sh|
          sh.add_row([ 'Foo', 'Bar' ])
        end
      end

      fpath = "#{@root}/aa.xlsx"
      package.serialize fpath

      Essing::Utils.unpack_file(fpath, hidden: false) do |path|
        expect(File.basename(path)).to eq("aa.csv")
        expect(File.read(path)).to eq("Foo,Bar\n")
      end
      expect(File.exist?("#{@root}/aa.csv")).to eq(false)
    end

    it "simple_argparse" do
      expect( Essing::Utils.simple_argparse([ "a", "b" ], {}) ).to eq([ [ "a", "b" ], {} ])
      expect( Essing::Utils.simple_argparse([ "a", "b", "--foo", "c" ], { foo: TrueClass }) ).to eq([ [ "a", "b", "c" ], {foo: true} ])
      expect( Essing::Utils.simple_argparse([ "a", "b", "--foo", "c" ], { foo: FalseClass }) ).to eq([ [ "a", "b", "c" ], {foo: false} ])
      expect( Essing::Utils.simple_argparse([ "a", "b", "--foo", "c" ], { foo: Integer }) ).to eq([ [ "a", "b" ], {foo: "c"} ])
      expect( Essing::Utils.simple_argparse([ "a", "b", "--foo", "c", "--bar" ], { foo: Integer, bar: TrueClass }) ).to eq([ [ "a", "b" ], { foo: "c", bar: true } ])
      expect( Essing::Utils.simple_argparse([ "a", "b", "--foo", "c", "--bar" ], { foo?: Integer, bar?: TrueClass }) ).to eq([ [ "a", "b" ], { foo: "c", bar: true } ])
      #expect( Essing::Utils.simple_argparse([ "a", "b", "--foo", "c" ], { foo: FalseClass }) ).to eq([ [ "a", "b", "c" ], {foo: false} ])
    end
  end
end
