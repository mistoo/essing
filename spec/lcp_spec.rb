# -*- coding: utf-8 -*-
require "test_helper"
require "essing/lcp"

describe "Essing" do
  describe "Lcp" do
    it 'join' do
      expect(Essing::Lcp.join(['foo x', 'foo y'], '|')).to eq('foo x|y')
    end

    it 'lcp' do
      expect(Essing::Lcp.lcp(['foo x', 'foo y'])).to eq "foo "
    end

    it "period" do
      expect(Essing::Lcp.period(['2020-01-01', '2020-01-10'])).to eq "2020.01.01-10"
      expect(Essing::Lcp.period(['2020-01-01', '2020-02-10'])).to eq "2020.01.01-02.10"
      expect(Essing::Lcp.period(['2020-01-01', '2021-01-01'])).to eq "2020.01.01-2021.01.01"
      expect(Essing::Lcp.period(['2020-01-01', '2021-02-02'])).to eq "2020.01.01-2021.02.02"
    end
  end
end
