# -*- coding: utf-8 -*-
require "test_helper"
require "essing"

describe "Essing" do
  describe "Errset" do
    it "err category" do
      errset = Essing::Errset.new(1)
      errset.add("message", cat: :foo)
      expect(errset.err_stats).to eq({ foo: 1 })
    end
  end
end
