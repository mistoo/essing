# -*- coding: utf-8 -*-
require "test_helper"
require "essing"

describe "Essing" do
  before :all do
    @is_valid = proc{ |e| Essing::Validator.email_valid?(e) }
    @email_valid = proc{ |e| "#{e} #{Essing::Validator.email_valid?(e)}" }
    @domain_valid = proc{ |e| "#{e} #{Essing::Validator.domain_valid?(e)}" }
  end

  describe "Validator" do
    it "valid domain" do
      [ 'www.foo.com', 'foo.com', 'foo.com.pl', 'foo.bar.com.pl', 'foo123.com', 'foo-bar.com.pl', 'a.foo-bar.com.pl', 'a.co', 'a.c.uk' ].each do |domain|
        expect(@domain_valid.(domain)).to eq("#{domain} true")
      end
    end

    it "invalid domain" do
      [ 'www-.foo.com', '-foo.com', 'foo.com.pl\\', 'foo.bar.com.pl-', 'foo123%.com', 'foo-bar.c,om.pl', 'a&.foo-bar.com.pl', 'foo.c', 'foo.c%', 'a.c', 'a.2c', 'a.2', 'a.c.u' ].each do |domain|
        expect(@domain_valid.(domain)).to eq("#{domain} false")
      end
    end

    it "email: @" do
      expect(@is_valid.('usera.pl')).to eq(false)
      expect(@is_valid.('usa@eraaapl')).to eq(false)
    end


    it "email:short domain" do
      expect(@is_valid.('user@a.p')).to eq(false)
      expect(@is_valid.('user@a.pl')).to eq(true)
    end

    it "email:non ascii" do
      expect(@is_valid.('userł@a.pl')).to eq(false)
    end

    it "email:non ascii start/end" do
      expect(@is_valid.('user_@foo.pl')).to eq(true)
      expect(@is_valid.('-user@foo.pl')).to eq(false)
      expect(@is_valid.('.user@foo.pl')).to eq(false)
      expect(@is_valid.('-user-@foo.pl')).to eq(false)
      expect(@is_valid.('user-@foo.pl')).to eq(false)
      expect(@is_valid.('user.@foo.pl')).to eq(false)
      expect(@is_valid.('user-user@foo.pl')).to eq(true)
      # EmailAddress.valid?('ogog@sss.c')
      #assert ! EmailAddress.valid?('example.user@foo.com/')
    end

    it "email:non existing domain" do
      addr = 'user@1233-1414-14314-4444-foo-bar-baz-131.com'
      expect(@is_valid.(addr)).to eq(true)
      expect(Essing::Validator.email_deep_valid?(addr)).to eq(false)
    end
  end
end
