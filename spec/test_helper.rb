$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

require 'tmpdir'
require 'fileutils'
require 'rubygems'

$tmpdir = "#{Dir.tmpdir}/essingtests"
FileUtils.rm_rf $tmpdir
FileUtils.mkdir_p $tmpdir

require 'essing'
require 'rspec'
