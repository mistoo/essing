# -*- coding: utf-8 -*-
require "test_helper"
require "essing"

describe "Essing" do
  describe "Spool" do
    before :each do
      @root = "#{$tmpdir}/essing-spool-testing"
      FileUtils.rm_rf(@root)
      FileUtils.mkdir_p(@root)
      @spool = Essing::Spool.new(@root)
    end

    it "empty spool" do
      expect(@spool.size).to eq(0)
      n = 0
      @spool.process do |entry|
        n += 1
      end
      expect(n).to eq(0)
    end

    it "non-empty spool" do
      expect(@spool.size).to eq(0)
      File.write("#{@spool.inputdir}/foo.txt", "bar")
      expect(@spool.size).to eq(1)

      n = 0
      @spool.process do |entry|
        expect(entry.filename).to eq("foo.txt")
        n += 1
      end
      expect(n).to eq(1)
    end

    it "do not process again" do
      expect(@spool.size).to eq(0)
      File.write("#{@spool.inputdir}/foo.txt", "bar")
      expect(@spool.size).to eq(1)

      @spool.process{ nil }
      expect(@spool.size).to eq(1)

      @spool.process{ false }
      expect(@spool.size).to eq(0)

      File.write("#{@spool.inputdir}/bar.txt", "foo")
      expect(@spool.size).to eq(1)

      @spool.process{ true }
      expect(@spool.size).to eq(0)
    end

    it "external input dir" do
      input = "#{$tmpdir}/essing-spool-testing-input-external"
      FileUtils.rm_rf(input)
      FileUtils.mkdir_p(input)
      @spool = Essing::Spool.new(@root, inputdir: input)
      expect(@spool.inputdir).to eq(input)
      File.write("#{input}/foo.txt", "bar")
      expect(@spool.size).to eq(1)
      n = 0
      @spool.process do |entry|
        n += 1
        true
      end
      expect(@spool.size).to eq(0)
      expect(n).to eq(1)
    end

    it "locking" do
      File.write("#{@spool.inputdir}/foo.txt", "bar")
      File.write("#{@spool.inputdir}/bar.txt", "foo")
      n = 0
      n2 = 0
      fail_re = nil
      expect(@spool.size).to eq(2)
      ok_re = @spool.process do |entry|
        fail_re = @spool.process{ n2 += 1}
        expect(fail_re).to eq(false)
        n += 1
        true
      end
      expect(@spool.size).to eq(0)

      expect(ok_re).to eq(true)
      expect(n).to eq(2)
      expect(n2).to eq(0)
    end

    it "feed_from_static" do
      File.write("#{$tmpdir}/foo.txt", "bar")
      expect(@spool.size).to eq(0)
      @spool.feed("#{$tmpdir}/foo.txt")
      expect(@spool.size).to eq(1)
      sleep 1
      FileUtils.touch("#{$tmpdir}/foo.txt")
      @spool.feed("#{$tmpdir}/foo.txt")
      expect(@spool.size).to eq(2)
    end
  end
end
