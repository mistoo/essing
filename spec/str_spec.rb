# -*- coding: utf-8 -*-
require "test_helper"
require "essing"

describe "Essing" do
  describe "Str" do
    it "to_ascii" do
      expect(Essing::Str.to_ascii("łódź")).to eq('lodz')
    end

    it "strip" do
      expect(Essing::Str.strip!("  a  la    ")).to eq('a la')
    end

    it "normalize" do
      expect(Essing::Str.normalize("łódź.fabryczna widz!.ew")).to eq('lodz_fabryczna_widz_ew')
    end

    it "strip_html_tags!" do
      [
        [ 'Ala<a>ma kota', 'Alama kota' ],
        [ 'Ala<a>ma</a> kota', 'Alama kota' ],
        [ 'Ala<a style="font-size: 1em">ma</a> kota', 'Alama kota' ],
        [ 'Ala&<a style="font-size: 1em">ma</a> kota', 'Alama kota' ],
        [ 'Ala<<a>ma kota', 'Alama kota' ],
        [ '2', '2' ],
        [ '21212.3', '21212.3' ]

      ].each do |src, dest|
        expect(Essing::Str.strip_html_tags!(src)).to eq(dest)
      end
    end
  end
end
