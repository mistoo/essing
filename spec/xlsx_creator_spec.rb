# -*- coding: utf-8 -*-
require "test_helper"
require "essing/xlsx_creator"

describe "Essing" do
  describe "XlsxCreator" do
    it "create" do
      path = Essing::XlsxCreator.create("/tmp/a.xlsx") do |sheet, styles|
        sheet.add_row([ "A", "B", "C" ])
      end
      expect(`xlsx2csv #{path}`).to eq ("A,B,C\n")
    end
  end
end
