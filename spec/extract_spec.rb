# -*- coding: utf-8 -*-
require "test_helper"
require "essing"

describe "Essing" do
  describe "Extract" do
    it "iso" do
      expect(Essing::Extract.date_from_string("foo2021-11-21")).to eq(Date.new(2021, 11, 21))
      expect(Essing::Extract.date_from_string("foo2021.11.21")).to eq(Date.new(2021, 11, 21))
      expect(Essing::Extract.date_from_string("foo2021_11_21")).to eq(Date.new(2021, 11, 21))
    end

    it "joined" do
      expect(Essing::Extract.date_from_string("foo20211121")).to eq(Date.new(2021, 11, 21))
    end

    it "reversed joined" do
      expect(Essing::Extract.date_from_string("foo20122021")).to eq(Date.new(2021, 12, 20))
    end

    it "reversed iso" do
      expect(Essing::Extract.date_from_string("foo21-11-2021")).to eq(Date.new(2021, 11, 21))
      expect(Essing::Extract.date_from_string("foo21.11.2021")).to eq(Date.new(2021, 11, 21))
      expect(Essing::Extract.date_from_string("foo21_11_2021")).to eq(Date.new(2021, 11, 21))
    end
  end
end
