# -*- coding: utf-8 -*-
require "test_helper"
require "essing"

describe "Essing" do
  describe "Mailer" do
    before :each do
      @mailer = Essing::Mailer.new(host: 'localhost', port: 25, domain: nil,
                                  username: nil, password: nil,
                                  tls: nil, debug: false,
                                  defaults: {})
      dir = ENV.fetch('TMPDIR', '/tmp')
      @root = "#{dir}/essing-test-mailer"
      FileUtils.rm_rf(@root)
      FileUtils.mkdir_p(@root)
    end

    it "render mail template" do
      path = "#{@root}/template.html.erb"
      File.write(path, "Hello <b><%= name %></b>")

      body = @mailer.render_mail_body(path, { name: 'world' })
      expect(body.class).to eq(String)
      expect(body).to eq('Hello <b>world</b>')
    end
  end
end
