# frozen_string_literal: true

module Essing
  module Refinements
    refine Integer do
      def ordinalize
        if (11..13).include?(self % 100)
          "#{self}th"
        else
          case self % 10
          when 1; "#{self}st"
          when 2; "#{self}nd"
          when 3; "#{self}rd"
          else    "#{self}th"
          end
        end
      end
    end

    # mimic ruby 3.x Symbol::name
    unless :a.respond_to?(:name)
      if :a.respond_to?(:to_fstr)
        Symbol.class_eval{ def name; to_fstr; end }
      else
        Symbol.class_eval{ def name; to_s; end }
      end
    end
    String.class_eval{ def name; self; end } unless ''.respond_to?(:name)

    refine Symbol do
      def start_with?(s)
        self.name.start_with?(s.name)
      end

      def end_with?(s)
        self.name.end_with?(s.name)
      end

      def include?(s)
        self.name.include?(s.name)
      end
    end

    refine String do
      def start_with?(s)
        super(s.name)
      end

      def end_with?(s)
        super(s.name)
      end

      def include?(s)
        super(s.name)
      end
    end
  end

  using Refinements
end
