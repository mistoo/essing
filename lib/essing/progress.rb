# frozen_string_literal: true

module Essing
  class Progress
    DEFAULT_STEP = 10

    def initialize(total, percent_step: DEFAULT_STEP, records_label: 'r')
      @total = total
      @t0 = Time.now

      percent_step ||= DEFAULT_STEP    # default 10%
      @step = (@total / percent_step).to_i
      @records_label = records_label
    end

    def human_eta(eta)
      eta_min = eta.to_i / 60
      eta_sec = eta.to_i % 60
      eta_human = eta_min.positive? ? "#{eta_min}m" : ''
      eta_human += eta_sec.positive? ? "#{eta_sec}s" : ''
      eta_human = '0' if eta_human.size.zero?
      eta_human
    end
    private :human_eta

    def eta(n, prefix = "", force: false, percent: false)
      m = nil

      if n > 0 && (n % @step == 0 || force)
        estimated = (Time.now - @t0) / (1.0 * n / @total)
        eta = estimated - (Time.now - @t0)

        eta_min = eta.to_i / 60
        eta_sec = eta.to_i % 60
        eta_human = eta_min > 0   ? "#{eta_min}m" : ''
        eta_human += eta_sec > 0  ? "#{eta_sec}s" : ''
        eta_human = '0' if eta_human.size.zero?

        message = "#{prefix} #{n}"
        if percent
          message = "#{prefix} #{n}/#{@total} (#{sprintf('%.1f%%', 100.0 * (n/@total.to_f))})"
        end

        speed = sprintf("%.1f #{@records_label}/s", n / (Time.now - @t0).to_f)

        m = "#{message}, eta #{eta_human}, speed #{speed}"
      end
      m
    end

    def humanize(secs)          #
      [[60, :s], [60, :m], [24, :h], [Float::INFINITY, :d]].map do |count, name|
        if secs > 0
          secs, n = secs.divmod(count)
          "#{n.to_i}#{name}" unless n.to_i.zero?
        end
      end.compact.reverse.join('')
    end
    private :humanize

    def human_elapsed
      humanize(Time.now - @t0)
    end
  end
end
