# frozen_string_literal: true

module Essing
  module RecordTransformer
    def self.one_of(rec, fields, as)
      value = fields.map{ |name| rec[name] }.first
      rec[as] = value
    end

    def self.merge(rec, fields, as)
      raise as.inspect if rec.has_key?(as)

      values = fields.collect{ |name| rec[name] }.compact
      rec[as] = values.size.zero? ? nil : values.join(' ')
    end

    def self.old_rename(rec, from, to, relax: false)
      unless relax
        raise "#{to}: key already exist (#{rec[to]})" if rec.has_key?(to)
        raise "missing key #{from.inspect} (avail keys: #{rec.keys.inspect})" unless rec.has_key?(from)
      end
      return unless rec.has_key?(from)

      rec["original__#{to}"] = rec.delete(to) if rec.has_key?(to)
      rec[to] = rec.delete(from)
    end

    def self.rename(rec, from, to, overwrite: false, required: false)
      # rubocop:disable Style/IfUnlessModifier
      if rec.has_key?(to) && !overwrite
        raise ArgumentError, "#{to}: key already exist (#{rec[to]})"
      end

      if required && !rec.has_key?(from)
        raise ArgumentError, "missing key #{from.inspect} (avail keys: #{rec.keys.inspect})"
      end
      # rubocop:enable Style/IfUnlessModifier

      return unless rec.has_key?(from)

      rec["original__#{to}"] = rec.delete(to) if rec.has_key?(to)
      rec[to] = rec.delete(from)
    end

    def self.transform(rec, transformations, relax: false)
      if transformations[:merge]
        transformations[:merge].each do |as, fields|
          merge(rec, fields, as)
        end
      end

      if transformations[:one_of]
        transformations[:one_of].each do |as, fields|
          one_of(rec, fields, as)
        end
      end

      if transformations[:rename] # rubocop:disable Style/GuardClause
        transformations[:rename].each do |from, to|
          old_rename(rec, from, to, relax: relax)
        end
      end
    end
  end
end
