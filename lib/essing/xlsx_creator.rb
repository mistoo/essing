if Kernel.respond_to? :autorequire
  autorequire :Axlsx, 'axlsx-alt'
else
  require "axlsx-alt"
end

require 'ostruct'

module Essing
  class XlsxCreator
    def initialize(path, font_size: 10)
      @path = path
      @font_size = font_size || 10
    end

    def workbook
      st = OpenStruct.new
      package = ::Axlsx::Package.new

      sz = @font_size
      package.workbook do |wb|
        styles = wb.styles
        st.caption = styles.add_style :b => false, :alignment => { :horizontal => :center }, :sz => sz
        st.header = styles.add_style :b => true, :alignment => { :horizontal => :center }, :sz => sz
        st.default  = styles.add_style :alignment => { :horizontal => :right }, :sz => sz
        st.right  = styles.add_style :alignment => { :horizontal => :right }, :sz => sz
        st.right_bold  = styles.add_style :b => true, :alignment => { :horizontal => :right }, :sz => sz
        st.text = styles.add_style :alignment => { :horizontal => :right, :wrap_text => true }, :sz => sz
        st.text_bold = styles.add_style :b => true, :alignment => { :horizontal => :right, :wrap_text => true }, :sz => sz
        st.small = styles.add_style :alignment => { :horizontal => :center, :wrap_text => true }, :sz => sz - 2
        st.number = styles.add_style format_code: "0.0", sz: sz
        st.money = wb.styles.add_style(:format_code => '0,0.00')
        st.float3 = wb.styles.add_style(:format_code => '0.000')
        st.int = wb.styles.add_style(:format_code => '0')
        st.percent = wb.styles.add_style(:format_code => '0.00%')
        st.open_text = styles.add_style :alignment => { :horizontal => :right, :wrap_text => true }, :sz => sz - 1

        yield wb, st
      end

      package.use_shared_strings = true
      package.serialize @path
      @path
    end

    def worksheet(name, headers: nil)
      workbook do |wb, st|
        wb.add_worksheet(name: name.to_s) do |sheet|
          sheet.add_row(headers, style: st.header) if headers
          yield sheet, st
        end
      end
    end

    def self.create(path, sheet_name: nil)
      XlsxCreator.new(path).worksheet(sheet_name) do |sheet, styles|
        yield sheet, styles
      end
    end
  end
end
