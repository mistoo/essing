# frozen_string_literal: true
require 'colorize'
require 'fileutils'
require 'charlock_holmes'

module Essing
  class Unpacker
    def initialize(tmpdir:, hidden:, delete: true, verbose: false, options: {})
      @tmpdir = tmpdir
      @hidden = if tmpdir && hidden.nil?
                  false # default to false with tmpdir
                elsif hidden.nil?
                  true  # default true
                end
      @verbose = verbose
      @options = options

      @options.each_key do |key|
        meth = "un#{key}".to_sym
        raise ArgumentError, "#{meth}: no such method" unless respond_to?(meth, :include_private)
      end
    end

    def unpackable_path(path, ext)
      to_path = path.sub(/\.#{ext}$/, '')
      return path if path == to_path

      bn = File.basename(to_path)
      dot = bn.start_with?('.') ? '' : '.'
      dot = "" unless @hidden

      if @tmpdir
        to_path = "#{@tmpdir}/#{bn}"
      else
        to_path = "#{File.dirname(to_path)}/#{dot}#{bn}"
      end
      to_path
    end
    private :unpackable_path

    UNPACKS = [ :pgp, :gpg, :bz2, :gz, :zip, '7z'.to_sym, :sav, :gz, :xlsx ]
    def unpack(path, &blk)
      tmp_paths = []
      UNPACKS.each do |ext|
        if (to_path = unpackable_path(path, ext.to_s)) != path
          meth = "un#{ext}".to_sym
          upath = send(meth, path, to_path, **(@options[ext] || {}))
          if upath != path
            tmp_paths.push(upath)
            path = upath
          end
        end
      end

      if blk
        re = blk.call(path)
        unless @delete == false
          tmp_paths.each{ |p| FileUtils.rm(p) rescue nil } # rubocop:disable Style/RescueModifier
        end
      else
        re = [path, tmp_paths]
      end
      re
    end

    private
    def ungz(path, to_path, options = {})
      Utils.system("gunzip -c #{path} > #{to_path}")
      to_path
    end

    def unbz2(path, to_path, options = {})
      Utils.system("bunzip2 -c #{path} > #{to_path}")
      to_path
    end

    def unzip(path, to_path, password: nil)
      cmd = [ "funzip" ]
      cmd << "-#{password}" if password
      cmd << "#{path} > #{to_path}"
      Utils.system(cmd.join(' '), "funzip #{path}")
      to_path
    end

    def un7z(path, to_path, password: nil)
      dir = File.dirname(path)
      tmpdir = "#{dir}/tmp.#{$$}"
      FileUtils.rm_f(tmpdir)

      cmd = [ "7z -y -o#{tmpdir}" ]
      cmd << "-p#{password}" if password
      cmd << "e #{path}"
      Utils.system(cmd.join(' '), "7z e #{path}")

      to_path
    end

    def unsav(path, to_path, options = {})
      to_path = "#{to_path}.csv.gz"
      cmd = "sav2csv #{path}"
      cmd = "#{cmd} --hidden" if @hidden
      Utils.system(cmd)
      raise "#{to_path}: no such file" unless File.exist?(to_path)
      to_path
    end

    def unpgp(path, to_path, options = {})
      ext = options.fetch(:ext, :pgp).to_s
      raise ArgumentError, "not #{ext}?" unless path.end_with?(ext)
      Utils.system("gpg --batch --yes -q --yes --decrypt #{path} > #{to_path}")
      to_path
    end

    def ungpg(path, to_path, options = {})
      unpgp(path, to_path, ext: '.gpg', **options)
    end

    def unxlsx(path, to_path, sheet: nil, sheet_name: nil, sheet_no: nil)
      to_path = "#{to_path}.csv"
      FileUtils.rm_f(to_path)

      sheet_sel = if sheet
                    " -n '#{sheet}'"
                  elsif sheet_name
                    " -n '#{sheet_name}'"
                  elsif sheet_no
                    " -s '#{sheet_no}'"
                  else
                    nil
                  end

      Utils.system("xlsx2csv #{path} #{to_path}#{sheet_sel} -f '%Y-%m-%d'", verbose: @verbose)
      raise "#{to_path}: no such file" unless File.exist?(to_path)

      to_path
    end
  end
end
