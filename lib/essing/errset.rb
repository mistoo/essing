# frozen_string_literal: true

module Essing
  class Errset
    attr_reader :errors, :warnings, :line_no, :err_stats

    def initialize(line_no)
      @line_no = line_no
      @errors = []
      @warnings = []
      @err_stats = Hash.new(0)
    end

    def each(type)
      if type == :error
        @errors.each{ |e| yield e }
      elsif type == :warning
        @warnings.each{ |e| yield e }
      else
        raise ArgumentError
      end
    end

    def add(msg, cat: nil)
      @errors.push(msg)
      @err_stats[cat] += 1 if cat
    end

    def add_warn(msg)
      @warnings.push(msg)
    end

    def empty?
      @errors.size.zero?
    end

    def not_empty?
      !empty?
    end

    def reset!(line_no = nil)
      @errors = []
      @warnings = []
      @err_stats = Hash.new(0)
      @line_no = line_no if line_no
    end

    def print(prefix = "")
      if @errors.size.positive?
        @errors.each do |m|
          puts "#{prefix}#{line_no}: #{m}"
        end
      end

      return unless @warnings.size.positive?

      @warnings.each do |m|
        puts "#{prefix}#{line_no}: warn: #{m}"
      end
    end
  end

  ErrSet = Errset               # backward compat
end
