# frozen_string_literal: true
require 'colorize'
require 'fileutils'
require 'charlock_holmes'

require_relative 'unpacker'
require_relative 'refinements'

module Essing
  module Utils
    using Refinements

    def self.colorize(str, color)
      if STDOUT.isatty == false
        str
      else
        str.to_s.colorize(color)
      end
    end

    def self.system(cmd, errmsg = nil, verbose: false)
      puts cmd if verbose
      r = Kernel.system(cmd)
      fail "#{errmsg || cmd} failed" unless r == true

      r
    end

    def self.system_diff(path, to_path)
      Kernel.system("diff -u0 #{path} #{to_path}")
    end

    def self.file_exists?(path, verbose: false)
      unless File.exist?(path)
        puts "#{path}: no such file found" if verbose
        return false
      end
      true
    end

    def self.ensure_file_exist!(path)
      raise ArgumentError, "#{path}: no such file" unless file_exists?(path)
    end

    def self.write_if_differ(path, body)
      if File.exist?(path)
        orig = File.read(path)
        return if orig == body
      end

      File.open(path, "wb") do |file|
        file.puts body
      end
    end

    def self.sniff_encoding(str, default: 'UTF-8')
      re = CharlockHolmes::EncodingDetector.detect(str)
      re ? re[:encoding] : default
    end

    def self.sniff_file_encoding(path, encoding: 'UTF-8')
      s = File.read(path, 128 * 1024)
      sniff_encoding(s, default: encoding)
    end

    def self.recode_file(path, from:, to: 'UTF-8')
      system("recode -i #{from.downcase}..#{to.downcase} #{path}")
    end

    def self.roo_list_xlsx_sheets(path)
      require 'roo'
      x = Roo::Excelx.new(path)
      re = {}
      x.sheets.each_with_index do |name, i|
        re[i+1] = name
      end
      re
    rescue
      nil
    end
    private_class_method :roo_list_xlsx_sheets

    def self.xlsx_list_sheets(path)
      re = roo_list_xlsx_sheets(path)
      if re.nil?
        cmd = "xlsx2csv -a #{path}  | grep -- ------ | grep -P -o '\\d+\\s\\-\\s.+' | sed -r 's|\\s+\\-\\s+|:|g'"
        re = {}
        IO.popen(cmd, 'r') do |stdout|
          lines = stdout.map{ |s| s.strip }
          lines.each do |line|
            no, name = line.strip.split(":", 2)
            if (no && name)
              re[no] = name
            end
          end
        end
      end
      re
    end

    UNPACKER_METHODS_MAP = {
      bunzip: :bz2,
      gunzip: :gz,
      unzip: :zip,
      u7zip: :'7z',
      sav2csv: :sav,
      xlsx2csv: :xlsx
    }

    def self.unpack_file(path, options = {}, &blk)
      tmpdir = options[:tmpdir]
      hidden = options[:hidden]
      delete = options[:delete]
      verbose = options[:verbose]
      verbose = false if verbose.nil?

      op = {}
      UNPACKER_METHODS_MAP.each do |k, v|
        if (mop = options[k])
          op[v] = mop
        end
      end

      un = Unpacker.new(tmpdir: tmpdir, hidden: hidden, delete: delete, verbose: verbose, options: op)
      un.unpack(path, &blk)
    end

    def self.filelock(path, fail_message: nil, exit_on_fail: true)
      status = 1
      File.open(path, File::RDWR | File::CREAT, 0o644) do |file|
        status = file.flock(File::LOCK_EX | File::LOCK_NB)
        if status == 0
          yield if block_given?
        else
          if fail_message.is_a?(Proc)
            fail_message.call
          elsif fail_message.is_a?(String)
            $stderr.puts(fail_message) # rubocop:disable Style/StderrPuts
          end
          exit! if exit_on_fail
        end
      end
      status == 0
    end

    def self.volatile_file(path)
      File.open(path, "wb") do |file|
        yield file
      end
      FileUtils.rm(path)
    end

    def self.simple_argparse_help(config)
      h = [ config[:__msg] || 'missing __msg' ]
      config.each do |k, v|
        next if k.start_with?('__')
        optional = false
        if k.end_with?('?')
          optional = true
          k = k.to_s.chop.to_sym
        end

        a = "--#{k}"
        if v != FalseClass && v != TrueClass
          a = "#{a} #{a.sub(/^\-\-/, '').upcase}"
        end
        a = "[#{a}]" if optional
        h.push(a)
      end

      h.join(' ')
    end

    def self.simple_argparse(argv, config)
      args = []
      options = {}
      take_arg = nil

      argv.each do |a|
        if a == '-h' || a == '--help'
          puts simple_argparse_help(config)
          exit
        end

        unless a.start_with?('--') # arg
          if take_arg
            options[take_arg.first] = a
            take_arg = nil
          else
            args.push(a)
          end
        else                    # op
          aname = a.sub(/^\-\-/, '').to_sym
          t = config[aname]
          t = config["#{aname}?".to_sym] if t.nil?

          raise ArgumentError, "#{a}: unknown option" if t.nil?

          if t == TrueClass
            options[aname] = true
          elsif t == FalseClass
            options[aname] = false
          else                # with argument
            take_arg = [ aname, t ]
          end
        end
      end
      [ args, options ]
    end
  end
end
