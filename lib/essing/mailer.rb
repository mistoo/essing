# frozen_string_literal: true
require "ostruct"

#require "erubis"    # lazy
#require "html2text" # lazy
#require "mailer"    # lazy

module Essing
  module ErbHelpers
    def self.include(path)
      File.read(path)
    end

    def self.legacy_nget
      lambda{ |n, texts| n==1 ? texts[0] :  n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? texts[1] : texts[2] }
    end

    # :reek:LongParameterList
    def self.i18n_nget(n, t0, t1, t2)
      n==1 ? t0 :  n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? t1 : t2
    end

    def self.hget(h, key)
      v = h[key]
      raise "empty #{key}" if v.nil? || v.empty?
      v
    end

    # %foo(a) replaced with a if macros contains %foo
    def self.process_perc_macros(s, macros)
      t = s.clone
      macros.each do |m|
        regex0 = Regexp.new('%' + m + '\\{([^\}]+)\\}')
        regex1 = Regexp.new('%' + m + '\\(([^\)]+)\\)')
        t.gsub!(regex0, '\1');
        t.gsub!(regex1, '\1');
        break unless t.include?('%') # no macros left in text
      end

      if t.include?('%')        # remove all other macros
        t.gsub!(/%\w+\{[^\}]+\}/, '')
        t.gsub!(/%\w+\([^\)]+\)/, '')
      end
      t
    end
  end

  class Mailer
    attr_reader :port, :host

    class Defaults
      attr_reader :from, :cc, :bcc, :data
      def initialize(from: nil, cc: nil, bcc: nil, data: {})
        @from = from
        @cc = cc
        @bcc = bcc
        @data = data || {}
      end

      def to_hash
        { from: @from, cc: @cc, bcc: @bcc, data: @data }
      end
    end

    # rubocop:disable Metrics/CyclomaticComplexity,Metrics/PerceivedComplexity
    def initialize(host: 'localhost', port: 25, domain: nil,
                   username: nil, password: nil,
                   tls: nil, debug: false,
                   defaults: {})

      config = {
        address: host,
        port: port || 25
      }

      config[:domain] = domain if domain
      config[:user_name] = username if username

      if password
        config[:password] = password
        if tls != false
          config[:openssl_verify_mode] = 'none'
          config[:enable_starttls_auto] = true
        end
      end

      if tls || (tls.nil? && config[:port] != 25)
        config[:tls] = true
        config[:openssl_verify_mode] = 'none'
      end

      config[:severity] = :debug if debug
      @mail_config = config
      @defaults = Defaults.new(**defaults)
      init_mail(config)
    end
    # rubocop:enable Metrics/CyclomaticComplexity,Metrics/PerceivedComplexity

    def init_mail(config)
      require 'mail' # lazy
      Mail.defaults do
        delivery_method :smtp, config
      end
    end
    private :init_mail

    def erb_helpers
      {
        nget: proc { |n, texts| n==1 ? texts[0] : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? texts[1] : texts[2] },
        i18n_nget: ErbHelpers.method(:i18n_nget)
      }
    end
    private :erb_helpers

    def render_mail_body(path, data, perc_macros: nil)
      raise ArgumentError, "data must be a Hash" unless data.is_a?(Hash)
      raise ArgumentError, "#{path}: no such file" unless File.exist?(path)

      tmpl = File.open(path, "r:UTF-8") { |f| f.read }
      require 'erubis'          # lazy
      erb = Erubis::Eruby.new(tmpl)

      data = data.merge(fn: erb_helpers)
      body = erb.result(data)
      raise "body cannot be empty" if body.nil?

      body = ErbHelpers.process_perc_macros(body, perc_macros) if perc_macros
      body
    end

    def render_mail(template_path, data, perc_macros: nil, subject: nil, body_wrapper_template: nil)
      data = @defaults.data.merge(data)

      body = render_mail_body(template_path, data, perc_macros: perc_macros)
      raise "empty mail body (template #{template_path})" if body.nil?

      if subject.nil?           # extract from template
        i = body.index("\n")
        raise "mail body need lines" if i.nil?

        subject = body[0..i]
        subject.strip!
        subject.sub!(/^<!--\s+/, '')
        subject.sub!(/\s+-->$/, '')
        raise "template #{template_path} must start with Subject:" unless subject.start_with?('Subject:')

        subject.sub!(/^Subject:\s+/, '')
        subject.strip!

        body = body[i+1..-1]
      end

      if body_wrapper_template # mail2sms
        erb = ::Erubis::Eruby.new(body_wrapper_template)
        body = erb.result(data.merge(body: body))
      end

      raise "mail body is too short (template #{template_path})" if body.size < 100
      raise "mail subject (#{subject}) is too short, must be at least 12 characters long" unless subject.size >= 12

      [ subject, body ]
    end

    def add_optout_header(mail, optout)
      raise ArgumentError, "optout must me a hash" unless optout.is_a?(Hash)

      oo_mail = optout[:email]
      oo_url = optout[:url]
      raise ArgumentError, "optout must contain both url and email" unless oo_mail && oo_url

      list_unsub = "<mailto:#{oo_mail}>, <#{oo_url}>"
      #List-Unsubscribe: <mailto:unsubscribe-espc-tech-12345N@domain.com>, <http://domain.com/member/unsubscribe/?listname=espc-tech@domain.com>
      mail.header['List-Unsubscribe'] = list_unsub
    end

    def add_x_headers(mail, x_headers)
      x_headers.each do |name, val|
        raise "#{name}: invalid X- header" unless name.start_with?('X-')

        mail.header[name] = val
      end
    end

    def make_message_id(seed, from:, msgid_prefix:)
      message_id = Digest::SHA1.hexdigest(seed + "t" + Time.now.strftime('%Y%m%d'))
      domain = from.gsub(/.+?@([a-z\.]+).+$/, '\1')
      prefix = msgid_prefix || "mail"
      "<#{prefix}.#{message_id}@#{domain}>"
    end

    def add_body(mail, body, with_text_part:, body_wrapper_template: nil)
      is_html = body.include?('<body>') || body.include?('<p>') || body.include?('<head>')
      if !is_html
        mail.body = body
      else
        #process_images(mail, tmpl_path, body)
        mail.html_part do
          content_type 'text/html; charset=UTF-8'
          body body
        end

        if with_text_part
          require "html2text"
          mail.text_part do
            content_type 'text/plain; charset=UTF-8'
            body Html2Text.convert(body)
          end
        end
      end
    end

    def make_mail(from: nil, to:, cc: nil, bcc: nil,
                  template_path: nil, data: nil,
                  subject: nil, body: nil,
                  msgid_prefix: nil,
                  x_headers: nil,
                  optout: nil, # { email:, url: }
                  perc_macros: nil,
                  with_text_part: false,
                  attachments: nil)

      from ||= @defaults.from
      cc ||= @defaults.cc
      bcc ||= @defaults.bcc

      raise ArgumentError, "missing from" if from.nil?

      to = "<#{to}>" unless to.include?('<')
      spec = {
        charset: 'utf-8',
        from: from,
        to: to
      }

      spec[:cc] = cc if cc
      spec[:bcc] = bcc if bcc

      if template_path
        data = data.merge(mailinfo: spec)
        subject, body = render_mail(template_path, data, perc_macros: perc_macros, subject: subject)
      end

      raise ArgumentError, "mail body is too short (template #{template_path})" if body.size < 3
      raise ArgumentError, "mail subject (#{subject}) is too short, must be at least 12 characters long" unless subject.size >= 3

      spec[:subject] = subject
      spec[:message_id] = make_message_id(body + to,
                                          from: from,
                                          msgid_prefix: msgid_prefix || @config[:domain].split('.').first || 'essingmail')

      mail = Mail.new(**spec)

      mail.charset = 'UTF-8'
      mail.header['X-Original-To'] = to

      add_x_headers(mail, x_headers) if x_headers
      add_optout_header(mail, optout) if optout
      add_body(mail, body, with_text_part: with_text_part)

      if attachments
        attachments.each do |path|
          mail.add_file(path)
        end
      end

      mail
    end

    def dump_mail(mail, full: false)
      puts "--------------------------------"
      puts "From: #{mail.from.join(', ')}"
      puts "To: #{mail.to.join(', ')}"
      puts "Subject: #{mail.subject}"

      return unless full

      if mail.html_part
        puts mail.html_part.to_s
        puts "-----------"
      end
      puts mail.text_part.to_s.gsub(/^\s*$/, '') if mail.text_part
      puts mail.body.to_s.gsub(/^\s*$/, '') if mail.body
      puts "--------------------------------"
    end

    def send(from:, to:, cc: nil, bcc: nil,
             subject: nil, body: nil,
             template_path: nil, data: nil,
             msgid_prefix: 'mail',
             x_headers: nil,
             optout: nil,
             with_text_part: false,
             dry: false,
             dump: false,
             attachments: nil
            )

      raise ArgumentError, "need template_path or body" if body.nil? && template_path.nil?
      raise ArgumentError, "need template_path along with data" if !template_path.nil? && data.nil?
      raise ArgumentError, "need body along with subject" if !body.nil? && subject.nil?

      mail = make_mail(from: from, to: to,
                       cc: cc, bcc: bcc,
                       template_path: template_path, data: data,
                       subject: subject, body: body,
                       msgid_prefix: msgid_prefix,
                       x_headers: x_headers,
                       optout: optout,
                       with_text_part: with_text_part,
                       attachments: attachments)
      if dry
        puts "dry mode - mail not send"
        dump_mail(mail, full: dump)
      else
        mail.deliver!
      end
    end
  end
end
