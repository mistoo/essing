# frozen_string_literal: true

require "colorize"

module Essing
  module Console
    def self.get_logger(verbosity: 0, logger: nil, output: $stdout)
      @logger ||= Essing::Console::Logger.new
      @logger.set_logger(logger) unless @logger.logger
      @logger.setop(:verbosity, verbosity) if verbosity > @logger.verbosity
      @logger.set_output(output) unless @logger.output
      @logger
    end

    def self.process_log(stream)
      lines = { error: [], warn: [], notice: [], info: [], all: [] }
      stream.rewind
      stream.each do |line|
        line = line.chomp
        if line.start_with?('error:')
          lines[:error].push(line)
        elsif line.start_with?('warn:')
          lines[:warn].push(line)
        elsif line.start_with?('notice:')
          lines[:notice].push(line)
        else
          lines[:info].push(line)
        end
        lines[:all].push(line)
      end
      lines
    end
    private_class_method :process_log

    def self.capture_log
      logstream = StringIO.new
      output = @logger.output
      @logger.set_output([ output, logstream ])
      yield logstream
      @logger.set_output(output)

      process_log(logstream)
    end

    class Logger
      attr_reader :verbosity, :logger, :output

      PRIS = [ :info, :warn, :notice, :error, :debug, :panic ]

      STD_LOGGER_PRI_MAP = {
        info: :info,
        warn: :warn,
        notice: :warn,
        error: :error,
        panic: :fatal
      }.freeze

      PRI_COLORS = {
        warn: :yellow,
        error: :light_red,
        panic: :red,
        notice: :green,
        debug: :light_magenta
      }.freeze

      def self.remove_pri(str)
        PRIS.each{ |pri| str = str.sub(/^#{pri}:\s+/, '') }
        str
      end

      def initialize(verbosity: 0, logger: nil, output: $stdout)
        @verbosity = verbosity || 0
        @output = output
        @extra_outputs = []
        @logger = logger
      end

      def setop(name, value)      # legacy
        raise ArgumentError unless name == :verbosity
        @verbosity = value
      end

      def set_output(stream)
        @output = stream
      end

      def set_logger(logger)
        @logger = logger
      end

      def call_log_proc(proc)
        proc.call
      rescue StandardError => e
        if $app&.production_env?
          "log() exception #{e} raised"
        else
          raise
        end
      end
      private :call_log_proc

      def do_log(msg_verbosity, priority, message = nil, color = nil)
        #puts "VERBOSITY #{self.hash} #{@verbosity} #{msg_verbosity} #{priority}"
        return unless msg_verbosity <= @verbosity

        if message.nil?
          message = yield
        elsif message.is_a?(Proc)
          message = call_log_proc
        end

        if message.include?("\n")
          message.split("\n").each do |line|
            do_log(verbosity, priority, line, color)
          end
          return
        end
        return if message.is_a?(String) && message.gsub(/\s+/, '').size.zero?

        color = PRI_COLORS[priority] if color.nil?

        pri = "#{priority}: "
        asterisk = "* "
        m = message.start_with?(pri) || message.start_with?(asterisk) ? message : "* #{message}"

        if @logger
          meth = STD_LOGGER_PRI_MAP[priority] || :unknown
          @logger.send(meth, message)
        else
          outputs = @output.is_a?(Array) ? @output : [ @output ]
          outputs.each do |stream|
            isatty = stream.isatty
            ml = m
            if isatty
              ml = ml.colorize(color) if color
              if priority == :panic
                puts "** PANIC **".colorize(:red)
                raise ml
              end
            else
              message = message[2..-1] if message.start_with?(asterisk)
              ml = "#{priority}: #{message}" unless message.start_with?(pri)
            end

            stream.puts(ml)
          end
        end

        @extra_outputs.each do |stream|
          stream.puts(m) if @output
        end
      end
      private :do_log

      def log(*args)
        self.do_log(0, :info, *args) { yield }
      end

      def log1(*args)
        do_log(1, :info, *args) { yield }
      end

      def log2(*args)
        do_log(2, :info, *args) { yield }
      end

      def log3(*args)
        do_log(3, :info, *args) { yield }
      end

      def info(*args)
        do_log(0, :info, *args) { yield }
      end

      def info1(*args)
        do_log(1, :info, *args) { yield }
      end

      def info2(*args)
        do_log(2, :info, *args) { yield }
      end

      def notice(*args)
        do_log(0, :notice, *args) { yield }
      end

      def notice1(*args)
        do_log(1, :notice, *args) { yield }
      end

      def error(*args)
        do_log(0, :error, *args) { yield }
      end

      def warn(*args)
        do_log(0, :warn, *args) { yield }
      end

      def warning(*args)
        warn(*args) { yield }
      end

      def error(*args)
        do_log(0, :error, *args) { yield }
      end

      def panic(*args)
        do_log(0, :panic, *args) { yield }
        exit 1
      end

      def do_log_debug(msg_verbosity, *args)
        msg_verbosity -= 1 if msg_verbosity > 0 && ENV['DEBUG'].to_i > 0
        do_log(msg_verbosity, :debug, *args) { yield }
      end

      def debug(*args)
        do_log_debug(1, *args) { yield }
      end

      def debug2(*args)
        do_log_debug(2, *args) { yield }
      end

      def debug3(*args)
        do_log_debug(3, *args) { yield }
      end

      def debug4(*args)
        do_log_debug(4, *args) { yield }
      end

      def devel(*args)
        do_log_debug(0, *args) { yield }
      end
    end
  end
end
