# frozen_string_literal: true
require 'iconv'

module Essing
  module Str
    # parses human interval like 3d, 6M, etc
    def self.parse_interval(s)
      raise ArgumentError, "#{s} (#{s.class}): is not a string" unless s.is_a?(String)
      raise ArgumentError, "#{s}: invalid value" unless s.match(/^\d+[smhdM]$/)

      period = s.chars.last
      v = s.to_i
      return v if v.zero?

      mul = case period
            when 's'
              1
            when 'm'
              60
            when 'h'
              3600
            when 'd'
              3600 * 24
            when 'M'
              (3600 * 24 * 30.42).to_i
            when 'y'
              (3600 * 24 * 365).to_i
            else
              raise ArgumentError, "#{s}: invalid format"
            end
      mul * v
    end

    def self.to_ascii(str)
      a = Iconv.iconv('ascii//translit//ignore', 'utf-8', str)
      a = a.first if a.is_a? Array
      a
    end

    REGEX_SPACE = /[[:space:]]/.freeze
    REGEX_BEGIN_SPACE = /^[[:space:]]+/.freeze
    REGEX_END_SPACE = /[[:space:]]+$/.freeze
    REGEX_MANY_SPACES = /[[:space:]]+/.freeze

    def self.strip!(str)
      str.sub!(REGEX_BEGIN_SPACE, '')
      str.sub!(REGEX_END_SPACE, '')
      str.gsub!(REGEX_MANY_SPACES, ' ')
      str
    end

    def self.normalize(str)
      s = to_ascii(str)
      strip!(s)
      s.downcase!
      #s.tr!(':', '_') # ':' cannot be replaced as it is used by "userload" (TODO)
      s.tr!('(', '_')
      s.tr!('/', '_')
      s.tr!('>', '_')
      s.tr!('<', '_')
      s.delete!(')')
      s.sub!(/^\"/, '')
      s.sub!(/\"$/, '')
      s.tr!('-', '_')
      s.tr!('.', ' ')
      s.tr!(',', ' ')
      s.tr!('?', '')
      s.tr!('!', ' ')
      s.gsub!(REGEX_MANY_SPACES, '_')
      s.gsub!(/_+/, '_')
      s.gsub!(/_+$/, '')
      s
    end

    REGEX_HTML_TAGS = /<("[^"]*"|'[^']*'|[^'">])*>/.freeze
    REGEX_HTML_CHARS = /[\&\\\<\>]+/
    def self.strip_html_tags!(str)
      str.gsub!(REGEX_HTML_TAGS, '')
      str.gsub!(REGEX_HTML_CHARS, '')
      str
    end

    def to_humanized_json(hash, pretty: false)
      JSON.generate(hash).sub(/\"(\w+)\":/, '\1')
    end
  end
end
