# frozen_string_literal: true
require_relative 'utils'

module Essing
  class Spool
    attr_reader :rootdir, :inputdir

    DIR_INPUT = :in
    DIR_PROCESSED = :processed
    DIR_INVALID = :invalid

    SPOOL_DIRS = [ DIR_INPUT, DIR_PROCESSED, DIR_INVALID ]

    class Entry
      attr_reader :filename, :path
      def initialize(filename, path, spool:)
        @spool = spool
        @filename = filename
        @path = path
      end

      def set_processed!
        @spool.mark_processed!(@path)
      end

      def set_invalid!
        @spool.mark_invalid!(@path)
      end

      def processed?
        @spool.send(:processed_file?, @filename)
      end

      def spooled?
        @dirname ||= File.dirname(@path)
        @dirname == @spool.inputdir
      end
    end

    def initialize(rootdir, inputdir: nil)
      raise ArgumentError, "#{rootdir}: no such root directory" unless Dir.exist?(rootdir)
      raise ArgumentError, "#{inputdir}: no such input directory" if inputdir && !Dir.exist?(inputdir)

      @rootdir = rootdir
      @inputdir = (inputdir || DIR_INPUT).to_s
      @tree = initialize_spool_dirtree(rootdir, inputdir)
    end

    def inputdir
      @tree[DIR_INPUT]
    end

    def initialize_spool_dirtree(rootdir, inputdir)
      tree = {}
      SPOOL_DIRS.each do |name|
        tree[name] = "#{rootdir}/#{name}"
      end
      tree[:in] = inputdir if inputdir

      tree.each_value do |path|
        FileUtils.mkdir_p(path)
      end
      tree
    end
    private :initialize_spool_dirtree

    def path_in(dirname, filename)
      dir = @tree[dirname]
      raise ArgumentError, "#{dirname}: no such dir?" if dir.nil?

      "#{dir}/#{filename}"
    end
    private :path_in

    def in?(dirname, filename)
      File.exist?(path_in(dirname, filename))
    end
    private :in?

    def mark!(path, basename, to)
      to_path = path_in(to, basename)
      raise ArgumentError, "#{path} and #{to_path} are the same" if to_path == path
      raise ArgumentError, "#{to_path} already exist" if File.exist?(to_path)

      if File.dirname(File.dirname(path)) != File.dirname(File.dirname(to_path))
        FileUtils.ln_s(path, to_path)
      else                      # same fs => internal link
        dir = File.basename(File.dirname(path))
        raise "internal error: #{dir} != #{File.basename(inputdir)}" if dir != File.basename(inputdir)
        FileUtils.ln_s("../#{dir}/#{basename}", to_path)
        #FileUtils.ln(path, to_path) # hard link maybe? to rethink
      end
    end
    private :mark!

    def mark_processed!(path)
      basename = File.basename(path)
      mark!(path, basename, DIR_PROCESSED) unless processed_file?(basename)
    end

    def mark_invalid!(path)
      basename = File.basename(path)
      mark!(path, basename, DIR_INVALID) unless processed_file?(basename)
    end

    def processed_file?(basename)
      in?(DIR_PROCESSED, basename) || in?(DIR_INVALID, basename)
    end
    private :processed_file?

    def size
      n = 0
      do_process{ n += 1; nil }
      n
    end

    def process(all: false, dry: false, &blk)
      re = Utils.filelock("#{@rootdir}/.lock", exit_on_fail: false, fail_message: nil) do
        do_process(yield_all: all, dry: dry, &blk)
      end
      re
    end

    def do_process(yield_all: false, dry: false)
      dir = @tree[DIR_INPUT]
      Dir.foreach(dir) do |filename|
        next if filename.start_with?('.')

        filename = filename.encode('UTF-8', 'ASCII', :force => true)
        path = "#{dir}/#{filename}"

        next if Dir.exist?(path)
        next unless File.exist?(path)
        next if !yield_all && processed_file?(filename)

        entry = Entry.new(filename, path, spool: self)
        re = yield entry

        unless dry
          if re == true
            entry.set_processed!
          elsif re == false
            entry.set_invalid!
          else
            #raise "entry processing result is unknown"
            # do nothing
          end
        end
      end
    end
    private :do_process

    def info
      counters = Counters.new(pending: 0, processed: 0, invalid: 0, total: 0)
      pending = []

      do_process(yield_all: true, dry: true) do |entry|
        counters.inc(:total)
        if in?(DIR_PROCESSED, entry.filename)
          counters.inc(:processed)
        elsif in?(DIR_INVALID, entry.filename)
          counters.inc(:invalid)
        else
          counters.inc(:pending)
          pending.push(entry.filename)
        end
      end

      {
        counters: counters,
        pending_entries: pending
      }
    end

    def feed(path)
      return unless File.exist?(path)

      mtime = File.mtime(path)
      ext = File.extname(path)
      basename = "#{File.basename(path, ext)}-#{mtime.strftime('%Y%m%d_%H%M%S')}#{ext}".downcase

      FileUtils.cp(path, "#{inputdir}/#{basename}") unless in?(DIR_INPUT, basename)
    end
  end
end
