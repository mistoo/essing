# frozen_string_literal: true
require 'date'

module Essing
  module Extract
    def self.date_from_string(s)
      s = File.basename(s) if s.include?('/')

      m = s.match(/(2\d{3})[\.\-_]*(\d{2})[\-\._]*(\d{2})/)
      if m.nil?                 # reversed?
        m = s.match(/([0123]\d)[\-\._]*(\d{2})[\.\-_]*(2\d{3})/)
        if m
          ma = m
          m = [ nil, ma[3], ma[2], ma[1] ]
        end
      end

      raise ArgumentError, "Unable to extract date from #{s} (match)" unless m

      begin
        dt = Date.new(m[1].to_i, m[2].to_i, m[3].to_i)
      rescue ArgumentError      # reversed date?
        m = s.match(/(\d{2})(\d{2})(2\d{3})/)
        raise ArgumentError, "Unable to extract date from #{s}" unless m
        dt = Date.new(m[3].to_i, m[2].to_i, m[1].to_i)
      end
      dt
    end

    def self.month_from_string(s)
      s = File.basename(s) if s.include?('/')
      m = s.match(/(2\d{3})(\d{2})/)
      m ||= s.match(/(2\d{3})[\.\-_](\d{2})/)
      raise ArgumentError, "Unable to extract month from #{s}" unless m

      Date.new(m[1].to_i, m[2].to_i, 1)
    end
  end
end
