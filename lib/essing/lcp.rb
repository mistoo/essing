# frozen_string_literal: true
module Essing
  module Lcp
    # longest common prefix
    def self.lcp(strings)
      return "" if strings.size.zero?
      min, max = strings.minmax
      index = min.size.times{ |i|
        break i if min[i] != max[i]
      }
      min[0...index]
    end

    def self.period(strings)
      fe = strings.first
      le = strings.last

      if fe.include?('-') || fe.size < 6
        r0 = fe.split('-')
        r1 = le.split('-')
      else
        r0 = fe.sub(/(.{4})(.{2})/, '\1-\2').split('-')
        r1 = le.sub(/(.{4})(.{2})/, '\1-\2').split('-')
      end

      l0 = []
      l1 = []

      #puts r0.inspect
      differ = false
      r0.each_with_index do |frag, i|
        l0.push(frag)
        if frag != r1[i] || differ
          l1.push(r1[i])
          differ = true
        end
      end

      l0.join('.') + '-' + l1.join('.')
    end

    def self.join(strings, sep)
      punct = " :-"
      has_punct = false

      strings = strings.map do |s|
        unless has_punct
          punct.each_char do |c|
            if s.include?(c)
              has_punct = true
              break
            end
          end
        end
        "#{s.to_s.strip} "
      end

      prefix = lcp(strings)

      if has_punct && prefix.size > 0
        #puts "prefix0 (#{prefix})"
        #if prefix[-1] == " "
        #prefix = prefix[0..-2]
        if !punct.include?(prefix[-1])
          prefix = ""
        end
      end

      #puts "prefix1 (#{prefix}) #{strings}"
      if prefix.size > 0 && strings.any?{ |s| s == prefix } # prefix is one of strings?
        #puts "prefix2 (#{prefix})"
        # cut last word
        a = prefix.strip.split(' ')
        a.pop
        new_prefix = a.join(' ')
        if new_prefix == prefix   # fail?
          prefix = ""
        else
          prefix = "#{new_prefix} "
        end
      end
      #puts "prefix3 (#{prefix})"
      if prefix.size.zero?
        return strings.join(sep)
      end
      #puts "XXX #{strings.inspect}"
      s = prefix + strings.collect{ |e| e.sub(prefix, '').strip }.join(sep)
      s.gsub!(" #{sep}", "")
      s
    end
  end
end
