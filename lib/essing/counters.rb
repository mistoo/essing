# frozen_string_literal: true
require 'terminal-table'

require_relative 'refinements'

module Essing
  # Simple counters class
  # st = Counters.new(:foo => 0, :bar => 0)
  # st.inc(:foo)
  # st.inc(:bar, 2)
  # st.value(:foo)
  #  => 1
  # st.value(:bar)
  #  => 2
  class Counters
    using Refinements
    def initialize(keys)
      if keys.is_a?(Array)
        @keys = {}
        keys.each{ |key| @keys[key.to_sym] = 0 }

      elsif keys.is_a?(Hash)
        @keys = {}
        keys.each do |key, val|
          raise ArgumentError, "#{key} must be a Symbol" unless key.is_a?(Symbol)
          raise ArgumentError, "#{key} value must be a Integer" unless val.is_a?(Integer)

          @keys[key] = val
        end
      else
        raise ArgumentError, "constructor argument must be a Hash or Array"
      end
    end

    def raw_data
      @keys.clone
    end

    def key?(key)
      raise ArgumentError, "#{key}: key must be a Symbol" unless key.is_a?(Symbol)

      @keys.key?(key)
    end
    alias_method :has_key?, :key?

    def merge!(stats)
      raise ArgumentError, "merge! argument must be a Counters" unless stats.is_a?(Counters)

      stats.raw_data.each do |k, v|
        @keys[k] ||= 0
        @keys[k] += v
      end
    end

    def inc(key, value = 1)
      raise ArgumentError, "#{key}: key must be a Symbol" unless key.is_a?(Symbol)
      raise IndexError, "#{key}: no such key" unless @keys.key?(key)

      @keys[key] += value
    end

    def set(key, value)
      raise ArgumentError, "#{key}: key must be a Symbol" unless key.is_a?(Symbol)
      raise IndexError, "#{key}: no such key" unless @keys.key?(key)

      @keys[key] = value
    end

    def get(key)
      raise ArgumentError, "#{key}: key must be a Symbol" unless key.is_a?(Symbol)
      raise IndexError, "#{key}: no such key" unless @keys.key?(key)

      @keys[key]
    end

    def any_non_zero?(*keys)
      keys.each do |key|
        return true if get(key) > 0
      end
      false
    end

    def value(key)
      get(key)
    end

    def terminal_table(options = {})
      keys = @keys
      keys = @keys.select{ |k| options[:keys].include?(k) } if options[:keys]

      rows = keys.select{ |_k, v| v > 0 }.map{ |k, v| [k.to_s.tr('_', ' '), v] }
      Terminal::Table.new(rows: rows)
    end

    def message(subject = nil, options = {})
      keys = @keys
      keys = @keys.select{ |k| options[:keys].include?(k) } if options[:keys]

      sep = options[:sep] || ', '

      subject = subject ? " #{subject} " : " "
      keys.select{ |_k, v| v > 0 }.map do |k, v|
        m = "#{v}#{subject}#{k.to_s.tr('_', ' ')}"
        subject = " "
        m
      end.join(sep)
    end
  end
end
