# frozen_string_literal: true

require 'uri'
require 'email_address'

module Essing
  module Validator
    DNS_HOST_REGEX = / [\p{L}\p{N}]+ (?: (?: \-{1,2} | \.) [\p{L}\p{N}]+ )*/x.freeze
    HOST_REGEXP = /\A #{DNS_HOST_REGEX} \z/x.freeze
    TOPLEVEL_DOMAIN_REGEXP = /\.[a-z]{2,}+$/.freeze
    START_AZ09_REGEXP = /^[a-z0-9]+/.freeze
    END_AZ09_REGEXP = /[a-z0-9_]$/.freeze

    DATETIME_MICROSEC_REGEXP = /^\d{2}\/\d{2}\/\d{4}\s+\d{2}\:\d{2}\:\d{2}\.\d+/.freeze
    DATETIME_REGEXP = /^\d{4}\-\d{2}\-\d{2}\s+\d{2}\:\d{2}\:\d{2}/.freeze
    DATE_REGEXP = /^\d{4}[\/\-]\d{2}[\/\-]\d{2}$/.freeze

    def self.datetime_valid?(value)
      return false if value.nil?

      DATETIME_MICROSEC_REGEXP.match(value) || DATETIME_REGEXP.match(value) ? true : false
    end

    def self.date_valid?(value)
      value && !DATE_REGEXP.match(value).nil?
    end

    # rubocop:disable Layout/EmptyLineAfterGuardClause
    def self.email_deep_valid?(value)
      return false unless value.is_a?(String)
      return false unless value.size > 6

      e = value.downcase
      email_valid?(e) && EmailAddress.valid?(e,
                                             host_validation: :mx,
                                             local_format: :relaxed)
    end

    def self.domain_valid?(value)
      return false unless value.is_a?(String)
      return false if value.size > 63 || value.size < 4
      e = value.downcase
      HOST_REGEXP.match(e) && TOPLEVEL_DOMAIN_REGEXP.match(e) ? true : false
    end

    # rubocop:disable Metrics/PerceivedComplexity
    def self.email_valid?(value)
      return false unless value.is_a?(String) && value.size > 6 && value.include?('@')

      e = value.downcase
      return false unless URI::MailTo::EMAIL_REGEXP.match(e)
      return false unless Str.to_ascii(e) == e

      local, domain = e.split('@', 2)
      return false unless domain_valid?(domain)
      return false unless START_AZ09_REGEXP.match(local) && END_AZ09_REGEXP.match(local)

      true
    end
    # rubocop:enable Metrics/PerceivedComplexity
    # rubocop:enable Layout/EmptyLineAfterGuardClause

    def self.mobile_valid?(value)
      value && value.sub(/^0+/, '').match(/^\d{9}$/) ? true : false
    end

    def self.integer_valid?(value)
      value.match(/^\d+$/)
    end
  end
end
