# frozen_string_literal: true

module Essing
  module DateManip
    class Month
      def initialize(s = nil)
        @d = Date.today
        if s.is_a?(Date)
          @d = s
        elsif s.is_a?(String)
          s = "#{s}-01" if s.split("-").size < 3
          @d = Date.parse(s)
        else
          raise ArgumentError, "#{s}: need Date|String"
        end
      end

      def to_s
        "#{@d.year}-#{sprintf("%.2d", @d.month)}"
      end

      def first_day
        Date.new(@d.year, @d.month, 1)
      end

      def last_day
        Date.new(@d.year, @d.month, -1)
      end

      def prev_month
        self.class.new(@d << 1)
      end

      def next_month
        self.class.new(@d >> 1)
      end

      def next_months(n)
        self.class.new(@d >> n)
      end

      def range
        Range.new(first_day, last_day)
      end
    end
  end
end
