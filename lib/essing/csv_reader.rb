# frozen_string_literal: true

require 'csv'
require 'rcsv'
require_relative 'refinements'
require_relative 'str'

module Essing
  # :reek:MissingSafeMethod
  # CSV record reader
  class CsvReader
    # CSV record reader
    using Refinements
    COLUMN_SEPARATORS = [',', "\t", '|', ';'].freeze

    DEFAULT_RCSV_OPTIONS = {
      header: :none,
      parse_empty_fields_as: :nil,
      column_separator: ',',
      nostrict: true,
      #output_encoding: 'UTF-8',
      buffer_size: 20 * 1024 * 1024
    }.freeze

    def initialize(path, col_sep: nil, column_separator: nil, skip_blanks: true, allow_duplicate_headers: true, multiline_values: false)
      @path = path

      @csv_options = { col_sep: ',', skip_blanks: true }.merge({ col_sep: col_sep, skip_blanks: skip_blanks })
      @rcsv_options = DEFAULT_RCSV_OPTIONS

      sep = col_sep || column_separator
      sep = self.class.sniff_column_separator(path) if sep.nil?
      @rcsv_options = DEFAULT_RCSV_OPTIONS.merge(column_separator: sep) if sep

      @options = {
        allow_duplicate_headers: allow_duplicate_headers,
        multiline_values: multiline_values
      }
      @headers = {}
    end

    def self.each(path, options = {})
      reader = self.new(path, **options)
      reader.each_rec do |rec, index|
        yield [ rec, index ]
      end
    end

    def self.sniff_column_separator(path)
      raise ArgumentError, "required path argument" unless path

      line = nil
      File.open(path, "r:bom|UTF-8") do |in_file|
        line = in_file.first
      end
      return nil if line.nil?

      snif = {}
      COLUMN_SEPARATORS.each do |sep|
        c = line.count(sep)
        #puts "#{s} => #{c}"
        snif[sep] = c unless c.zero?
      end

      #puts snif.inspect
      snif = snif.sort{ |a, b| b[1] <=> a[1] }
      snif.size.positive? ? snif[0][0] : nil
    end

    def self.normalize_header(text, nth)
      return "xcol_#{nth}".to_sym if text.nil?
      str = Str.normalize(text)
      str.to_sym
    end

    # :reek:NestedIterators
    def test_csv!(path)
      # test UTF-8 AS CSV raises exception...
      File.open(path, "r:bom|UTF-8") do |file|
        lineno = 0
        file.each_line do |line|
          lineno += 1
          begin
            line.gsub("foobarbaz", "FOOBARBAZ")
          rescue ArgumentError => e
            puts "ERR #{lineno}: #{e}"
            puts line
            raise
          end
        end
      end
    end

    # :reek:BooleanParameter
    # :reek:ControlParameter
    # :reek:NestedIterators
    def self.get_headers(path, sep: nil, normalize: true, skip_blanks: true)
      sep = sniff_column_separator(path) if sep.nil?
      op = DEFAULT_RCSV_OPTIONS
      op = op.merge(column_separator: sep) if sep && sep != op[:column_separator]

      headers = []
      File.open(path, "r:bom|UTF-8") do |in_file|
        Rcsv.parse(in_file, op) do |row|
          next if row.all?{ |e| e.nil? } # skip blank rows
          headers = row.map.with_index{ |e, i| normalize ? normalize_header(e, i) : (e ? e : "xcol_#{i}") }
          break
        end
      end

      if skip_blanks
        while headers.last.start_with?('xcol_')
          headers.pop
        end
      end

      headers
    end

    def extract_headers
      headers = self.class.get_headers(@path, sep: @rcsv_options[:column_separator])

      dup_counts = Hash.new(-1)
      headers.map! do |key|
        raise "dup!" if key.include?('__')

        suffix = dup_counts[key] += 1

        raise ArgumentError, "#{@path}: duplicate header #{key.inspect}" if !@options[:allow_duplicate_headers] && suffix.positive?
        key = "#{key}__#{suffix}".to_sym if suffix.positive?
        key
      end
      headers
    end

    # :reek:UtilityFunction
    def cleanup_value(val)
      #puts "#{val.inspect} => "
      val.sub!(/^[\'\"]+/, '')
      val.sub!(/[\"\']+$/, '')
      val.sub!(/\s+/, ' ')

      if @options[:multiline_values] && val.include?("\n")
        val = val.split("\n")
        val.map!{ |s| Str.strip!(s) }
        val.compact!
        val = val.join("\n")
      else
        val = Str.strip!(val)
      end
      #puts " => #{val.inspect}"
      val = nil if val.size.zero?
      val
    end
    private :cleanup_value

    # :reek:NestedIterators
    def each_with_index
      #test_csv!(@path)
      headers = extract_headers
      File.open(@path, "r:bom|UTF-8") do |in_file|
        lineno = 0
        Rcsv.parse(in_file, @rcsv_options) do |row|
          next if row.all?{ |e| e.nil? } # skip empty lines

          lineno += 1
          next if lineno == 1 # skip headers

          rec = {}
          #puts "----#{lineno}-------"
          headers.zip(row).each do |key, val|
            val = cleanup_value(val) unless val.nil?
            rec[key] = val
          end
          next if rec.values.compact.size.zero? # skip empty records

          yield rec, lineno
        end
      end
    end
    alias each_rec each_with_index

    def make_headers(row)
      row.each_with_index do |col, i|
        next if col.nil?

        h = self.class.normalize_header(col, i)
        #puts "#{i}. #{col} => #{h}"
        @headers[h.to_sym] = i
      end
    end
    private :make_headers

    # :reek:NestedIterators
    def each_with_index_std_csv
      headrow = nil
      lineno = 0
      CSV.foreach(@path, @csv_options) do |row|
        lineno += 1
        if headrow.nil?
          headrow = row
          make_headers(row)
          next
        end
        rec = {}
        @headers.each do |col, i|
          v = row[i]
          if v
            v = v.strip
            v = nil if v.size.zero?
          end
          rec[col] = v
        end
        yield rec, lineno
      end
    end
  end
end
