# coding: utf-8

lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'essing/version'

Gem::Specification.new do |spec|
  spec.name          = 'essing'
  spec.version       = Essing::VERSION
  spec.authors       = ['mis@pld-linux.org']
  spec.email         = ['mis@pld-linux.org']
  spec.summary       = 'Data procEssing Utility library'
  spec.description   = ''
  spec.homepage      = 'http://bitbucket.org/mistoo/essing'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.required_ruby_version = '>= 2.3.0'
  #spec.add_runtime_dependency "html2text" # disabled due to nokogiri build error on ruby 2.6
  spec.add_runtime_dependency 'erubis'
  spec.add_runtime_dependency 'mail'
  spec.add_runtime_dependency 'roo'
  spec.add_runtime_dependency "ostruct"
  spec.add_runtime_dependency 'charlock_holmes'
  spec.add_runtime_dependency 'colorize'
  spec.add_runtime_dependency 'csv'
  spec.add_runtime_dependency 'email_address'
  spec.add_runtime_dependency 'fileutils'
  spec.add_runtime_dependency 'iconv'
  spec.add_runtime_dependency 'rcsv'
  spec.add_runtime_dependency 'terminal-table'
  spec.add_runtime_dependency 'uri'
  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'axlsx-alt'
end
